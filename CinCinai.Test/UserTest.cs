﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CinCinai.BLL;
using CinCinai.DAL;
using System.Data;

namespace CinCinai.Test
{
  /// <summary>
  /// Summary description for UserTest
  /// </summary>
  [TestClass]
  public class UserTest
  {
    private UserLogic userLogic;
    private RoleLogic roleLogic;

    public UserTest()
    {
      Initialize();
    }

    [TestInitialize]
    public void Initialize()
    {
      userLogic = new UserLogic();
      roleLogic = new RoleLogic();
    }

    private TestContext testContextInstance;

    /// <summary>
    ///Gets or sets the test context which provides
    ///information about and functionality for the current test run.
    ///</summary>
    public TestContext TestContext
    {
      get
      {
        return testContextInstance;
      }
      set
      {
        testContextInstance = value;
      }
    }

    #region Additional test attributes
    //
    // You can use the following additional attributes as you write your tests:
    //
    // Use ClassInitialize to run code before running the first test in the class
    // [ClassInitialize()]
    // public static void MyClassInitialize(TestContext testContext) { }
    //
    // Use ClassCleanup to run code after all tests in a class have run
    // [ClassCleanup()]
    // public static void MyClassCleanup() { }
    //
    // Use TestInitialize to run code before running each test 
    // [TestInitialize()]
    // public void MyTestInitialize() { }
    //
    // Use TestCleanup to run code after each test has run
    // [TestCleanup()]
    // public void MyTestCleanup() { }
    //
    #endregion

    [TestCleanup]
    public void CleanUp()
    {
      
    }

    [TestMethod]
    public void TestCreateUser()
    {
      user userTest = new user
      {
        name = "Matias",
        lastName = "Guillen",
        location = "N/A",
        phone = "892121",
        DNI = "1920112",
        role = roleLogic.get(1),
        roleId = 3
      };
      bool? isCreated = userLogic.create(userTest);
      Assert.IsTrue(isCreated.Value);
    }

    [TestMethod]
    public void TestUpdateUser()
    {
      user userTest = userLogic.get(5);
      userTest.name = "Alejandro";
      bool? isCreated = userLogic.update(userTest);
      Assert.IsTrue(isCreated.Value);
    }

    [TestMethod]
    public void TestDeleteUser()
    {
      user userTest = userLogic.get(5);
      bool? isDeleted = userLogic.delete(userTest);
      Assert.IsTrue(isDeleted.Value);
    }

    [TestMethod]
    public void TestGetUsers()
    {
      List<user> users = userLogic.get();
      Assert.IsTrue(users.Count > 0);
    }

  }
}
