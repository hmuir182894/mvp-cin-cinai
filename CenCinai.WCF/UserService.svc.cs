﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using CinCinai.BLL;
using CinCinai.DAL;
using Newtonsoft.Json;

namespace CenCinai.WCF
{
  // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "UserService" in code, svc and config file together.
  // NOTE: In order to launch WCF Test Client for testing this service, please select UserService.svc or UserService.svc.cs at the Solution Explorer and start debugging.
  public class UserService : IUserService
  {
    private UserLogic userLogic;

    public UserService()
    {
      userLogic = new UserLogic();
    }

    public string Get(int id)
    {
      user user = userLogic.get(id);
      string userJSON = JsonConvert.SerializeObject(user);
      return userJSON;
    }

    public CompositeType GetDataUsingDataContract(CompositeType composite)
    {
      if (composite == null)
      {
        throw new ArgumentNullException("composite");
      }
      if (composite.BoolValue)
      {
        composite.StringValue += "Suffix";
      }
      return composite;
    }


    public string Post(int id)
    {
      throw new NotImplementedException();
    }

    public string Put(int id)
    {
      throw new NotImplementedException();
    }

    public string Delete(int id)
    {
      throw new NotImplementedException();
    }
  }
}
