﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CinCinai.DAL;

namespace CinCinai.BLL
{
  public class RoleLogic
  {
    private RoleDAL roleDAL;

    public RoleLogic()
    {
      roleDAL = new RoleDAL();
    }

    public role get(int id)
    {
      role role = roleDAL.getRole(id);
      return role;
    }
  }
}
