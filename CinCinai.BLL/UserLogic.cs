﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CinCinai.DAL;
using System.Data.EntityClient;

namespace CinCinai.BLL
{
  public class UserLogic
  {

    private UserDAL userDAL;

    public UserLogic()
    {
      userDAL = new UserDAL();
    }

    public bool? create(user user)
    {
      try
      {
        if (!user.Equals(null))
        {
          int isAdded = userDAL.addUser(user);
          if (isAdded != 0)
            return true;
          return false;
        }
        return false;
      } 
      catch 
      {
        return false;
      }
    }

    public bool? update(user user)
    {
      try
      {
        user userCompared = userDAL.getUser(user.id);
        if (!userCompared.Equals(null))
        {
          int isUpdated = userDAL.updateUser(user);
          if (isUpdated != 0)
            return true;
          return false;
        }
        return false;
      }
      catch(Exception ex)
      {
        return false;
      }
    }

    public bool? delete(int id)
    {
      try
      {
        user userRemove = userDAL.getUser(id);
        if (!userRemove.Equals(null))
        {
          int isDeleted = userDAL.deleteUser(userRemove);
          if (isDeleted != 0)
            return true;
          return false;
        }
        return false;
      }
      catch
      {
        return false;
      }
    }

    public user get(int id)
    {
      user user = userDAL.getUser(id);
      return user;
    }

    public List<user> get()
    {
      List<user> users = userDAL.get();
      return users;
    }
  }
}
