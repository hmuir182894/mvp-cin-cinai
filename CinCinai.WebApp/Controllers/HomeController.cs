﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CinCinai.DAL;
using Newtonsoft.Json;

namespace CinCinai.WebApp.Controllers
{
    public class HomeController : Controller
    {
      private UserServiceReference.UserServiceClient userServiceClient;

      public HomeController()
      {
        userServiceClient = new UserServiceReference.UserServiceClient();
      }

      public ActionResult Index()
      {
        string userParse = userServiceClient.Get(2);
        user user = JsonConvert.DeserializeObject<user>(userParse);
        ViewBag.Message = "Index View, Hi " + user.name;
        return View(user);
      }

      public ActionResult About()
      {
          ViewBag.Message = "Your app description page.";

          return View();
      }

      public ActionResult Contact()
      {
          ViewBag.Message = "Your contact page.";

          return View();
      }
    }
}
