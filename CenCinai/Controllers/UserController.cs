﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using Newtonsoft.Json;
using CinCinai.BLL;
using CinCinai.DAL;
using System.Net;

namespace CenCinai.Controllers
{
    public class UserController : ApiController
    {
      private UserLogic userLogic;

      public UserController()
      {
        userLogic = new UserLogic();
      }

      // GET api/values
      public IEnumerable<user> Get()
      {
        List<user> users = userLogic.get();
        return users;
      }

      // GET api/values/5
      public string Get(int id)
      {
        user user = userLogic.get(id);
        string userJSON = JsonConvert.SerializeObject(user);
        return userJSON;
      }

      // POST api/values
      public void Post([FromBody]user user)
      {
        if (!ModelState.IsValid)
          throw new HttpResponseException(HttpStatusCode.BadRequest);
        
        bool? isCreated = userLogic.create(user);
        if (!isCreated.Value)
          throw new HttpResponseException(HttpStatusCode.BadRequest);
      }

      // PUT api/values/5
      public void Put(int id, [FromBody]user user)
      {
        if (!ModelState.IsValid)
          throw new HttpResponseException(HttpStatusCode.BadRequest);

        bool? isUpdated = userLogic.update(user);
        if (!isUpdated.Value)
          throw new HttpResponseException(HttpStatusCode.BadRequest);
      }

      // DELETE api/values/5
      public void Delete(int id)
      {
        if (!ModelState.IsValid)
          throw new HttpResponseException(HttpStatusCode.BadRequest);

        bool? isUpdated = userLogic.delete(id);
        if (!isUpdated.Value)
          throw new HttpResponseException(HttpStatusCode.BadRequest);
      }
    }
}
