﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CinCinai.DAL
{
  public class UserDAL
  {
    public int addUser(user user) 
    {
      ConstantsDAL.db.users.Add(user);
      return ConstantsDAL.db.SaveChanges();
    }

    public int updateUser(user user)
    {
      ConstantsDAL.db.users.Attach(user);
      ConstantsDAL.db.Entry(user).State = EntityState.Modified;
      return ConstantsDAL.db.SaveChanges();
    }

    public int deleteUser(user user)
    {
      ConstantsDAL.db.users.Remove(user);
      return ConstantsDAL.db.SaveChanges();
    }

    public user getUser(int id) 
    {
      user user = ConstantsDAL.db.users.Where(u => u.id == id).FirstOrDefault();
      return user;
    }

    public List<user> get()
    {
      List<user> users = ConstantsDAL.db.users.ToList();
      return users;
    }
  }
}
